# damBreak_avr

A simple casefile for sanity-checking what openFoam's fieldAverage function object is doing.

The damBreak tutorial at t=.5s:
![Alt text](t.5.png)

Same time with an averaging window of .5s
![Alt text](t.5avr.png)
